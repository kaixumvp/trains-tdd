package com.heiiyo.graph;

import com.heiiyo.city.City;
import com.heiiyo.city.impl.BaseCity;
import com.heiiyo.exception.NoSuchCityException;
import com.heiiyo.exception.NoSuchRouteException;
import com.heiiyo.relation.Relation;
import com.heiiyo.relation.impl.BaseRelation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7
 */
public class GraphicTest {

    private Graphic graphic;


    @BeforeEach
    public void beforeBuild(){
        List<City> cities = new ArrayList<>();
        List<Relation> relations = new ArrayList<>();
        createData(cities, relations);
        graphic = GraphicBuilder.build(cities, relations);
        Assertions.assertNotNull(graphic);
    }

    @Test
    public void  multiCityDistanceTest() throws NoSuchCityException, NoSuchRouteException {

        int distanceFormABC = graphic.multiCityDistance("A", "B", "C");
        Assertions.assertEquals(distanceFormABC, 9);

        int distanceFormAD = graphic.multiCityDistance("A", "D");
        Assertions.assertEquals(distanceFormAD, 5);

        int distanceFormADC = graphic.multiCityDistance("A", "D", "C");
        Assertions.assertEquals(distanceFormADC, 13);

        int distanceFormAEBCD = graphic.multiCityDistance("A", "E", "B", "C", "D");
        Assertions.assertEquals(distanceFormAEBCD, 22);

        Assertions.assertThrows(NoSuchRouteException.class, () -> graphic.multiCityDistance("A", "E", "D"), "NO SUCH ROUTE");

    }

    @Test
    public void  routeNumberTowCityMaximumTest() throws NoSuchCityException {
        int routeNumber = graphic.routeNumberMaximumStop("C", "C", 3);
        Assertions.assertEquals(routeNumber, 2);
    }

    @Test
    public void routeNumberTowCityExactlyTest() throws NoSuchCityException {
        int distance = graphic.routeNumberExactlyStop("A", "C", 4);
        Assertions.assertEquals(distance, 3);
    }

    @Test
    public void shortDistanceTowCityTest() throws NoSuchCityException {
        int shortDistanceAD = graphic.shortDistance("A", "C");
        Assertions.assertEquals(shortDistanceAD, 9);

        int shortDistanceBB = graphic.shortDistance("B", "B");
        Assertions.assertEquals(shortDistanceBB, 9);
    }

    @Test
    public void LessThanDistanceTowCityTest() throws NoSuchCityException {
        int routeNumber = graphic.routeNumberForDistanceLessThanValue("C", "C", 30);
        Assertions.assertEquals(routeNumber, 7);
    }


    private void createData(List<City> cities, List<Relation> relations){
        City cityA = new BaseCity("A");
        City cityB = new BaseCity("B");
        City cityC = new BaseCity("C");
        City cityD = new BaseCity("D");
        City cityE = new BaseCity("E");
        cities.add(cityA);
        cities.add(cityB);
        cities.add(cityC);
        cities.add(cityD);
        cities.add(cityE);

        Relation relationAB = new BaseRelation(cityA, cityB, 5);
        Relation relationBC = new BaseRelation(cityB, cityC, 4);
        Relation relationCD = new BaseRelation(cityC, cityD, 8);
        Relation relationDC = new BaseRelation(cityD, cityC, 8);
        Relation relationDE = new BaseRelation(cityD, cityE, 6);
        Relation relationAD = new BaseRelation(cityA, cityD, 5);
        Relation relationCE = new BaseRelation(cityC, cityE, 2);
        Relation relationEB = new BaseRelation(cityE, cityB, 3);
        Relation relationAE = new BaseRelation(cityA, cityE, 7);
        relations.add(relationAB);
        relations.add(relationBC);
        relations.add(relationCD);
        relations.add(relationDC);
        relations.add(relationDE);
        relations.add(relationAD);
        relations.add(relationCE);
        relations.add(relationEB);
        relations.add(relationAE);
    }
}
