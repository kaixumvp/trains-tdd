package com.heiiyo.city.impl;

import com.heiiyo.city.City;

import java.util.Objects;

public class BaseCity implements City {
    private String cityName;

    public BaseCity(String cityName) {
        this.cityName = cityName;
    }

    @Override
    public String getCityName() {
        return cityName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaseCity baseCity = (BaseCity) o;
        return Objects.equals(cityName, baseCity.cityName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cityName);
    }
}
