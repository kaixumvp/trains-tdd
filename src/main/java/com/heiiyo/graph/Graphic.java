package com.heiiyo.graph;

import com.heiiyo.city.City;
import com.heiiyo.exception.NoSuchCityException;
import com.heiiyo.exception.NoSuchRouteException;
import com.heiiyo.relation.Relation;

public interface Graphic {

    int multiCityDistance(String... cityNames) throws NoSuchCityException, NoSuchRouteException;

    int routeNumberMaximumStop(String c, String c1, int i) throws NoSuchCityException;

    int routeNumberExactlyStop(String a, String d, int i) throws NoSuchCityException;

    int shortDistance(String a, String c) throws NoSuchCityException;

    int routeNumberForDistanceLessThanValue(String a, String c, int i) throws NoSuchCityException;

    City getCityByName(String cityName);

    Relation getRelationByCities(City city, City pop);
}
