package com.heiiyo.graph;

import com.heiiyo.city.City;
import com.heiiyo.graph.impl.BaseGraphic;
import com.heiiyo.relation.Relation;

import java.util.List;

public class GraphicBuilder {

    public static Graphic build(List<City> cities, List<Relation> relations){
        return new BaseGraphic(cities, relations);
    }
}
