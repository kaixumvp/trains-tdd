package com.heiiyo.graph.impl;

import com.heiiyo.city.City;
import com.heiiyo.exception.NoSuchCityException;
import com.heiiyo.exception.NoSuchRouteException;
import com.heiiyo.graph.Graphic;
import com.heiiyo.graph.type.ComparableEnum;
import com.heiiyo.relation.Relation;

import java.util.*;
import java.util.stream.Collectors;

public class BaseGraphic implements Graphic {

    List<City> cities;
    List<Relation> relations;

    public BaseGraphic(List<City> cities, List<Relation> relations) {
        this.cities = cities;
        this.relations = relations;
    }

    @Override
    public int multiCityDistance(String... cityNames) throws NoSuchCityException, NoSuchRouteException {
        List<City> citiesPath = new LinkedList<>();
        for(String cityName : cityNames){
            City city = this.getCityByName(cityName);
            if (city == null)
                throw new NoSuchCityException();
            citiesPath.add(city);
        }
        City cityA = citiesPath.get(0);
        citiesPath.remove(0);
        Integer distance = 0;
        return calculateDistance(cityA, citiesPath, distance);
    }

    public int calculateDistance(City city, List<City> citiesPath, Integer distance) throws NoSuchRouteException {
        City pop = citiesPath.get(0);
        citiesPath.remove(0);
        Relation relation = this.getRelationByCities(city, pop);
        if (relation == null)
            throw new NoSuchRouteException();
        distance = relation.getDistance() + distance;
        if (citiesPath.size() <= 0)
            return distance;
        return calculateDistance(pop, citiesPath, distance);
    }

    @Override
    public int routeNumberMaximumStop(String c, String c1, int i) throws NoSuchCityException {
        Map<String, Integer> routeMap = new HashMap<>();
        routeMap.put("routeNumber", 0);
        City cityA = this.getCityByName(c);
        City cityB = this.getCityByName(c1);
        if (cityA == null || cityB == null)
            throw new NoSuchCityException();
        this.routeNumber(cityA, cityB, 0, i, routeMap, ComparableEnum.LESS);
        return routeMap.get("routeNumber");
    }

    private void routeNumber(City cityA, City cityB, int depth, int i, Map<String, Integer> routeNumber, ComparableEnum comparableEnum){
        ++depth;
        List<Relation> relationFilter = relations.stream().filter(relation -> relation.getCityA().equals(cityA))
                .collect(Collectors.toList());
        if (relationFilter == null || relationFilter.size() <= 0)
            return;
        List<City> relationCityBs = relationFilter.stream().map(Relation::getCityB).collect(Collectors.toList());
        List<City> notUserCities = new ArrayList<>();
        for (City itemCity: relationCityBs){
            if (itemCity.equals(cityB) ){
                if (comparableEnum == ComparableEnum.LESS && depth <= i){
                    routeNumber.put("routeNumber", routeNumber.get("routeNumber")+1);
                }
                if (comparableEnum == ComparableEnum.EQUSE && depth == i){
                    routeNumber.put("routeNumber", routeNumber.get("routeNumber")+1);
                }
            }
        }
        if (depth >= i){
            return;
        }
        for (City notUserCity : relationCityBs){
            this.routeNumber(notUserCity, cityB, depth, i, routeNumber, comparableEnum);
        }
        return;
    }

    @Override
    public int routeNumberExactlyStop(String a, String d, int i) throws NoSuchCityException {
        Map<String, Integer> routeMap = new HashMap<>();
        routeMap.put("routeNumber", 0);
        City cityA = this.getCityByName(a);
        City cityB = this.getCityByName(d);
        if (cityA == null || cityB == null)
            throw new NoSuchCityException();
        this.routeNumber(cityA, cityB, 0, i, routeMap, ComparableEnum.EQUSE);
        return routeMap.get("routeNumber");
    }

    @Override
    public int shortDistance(String a, String c) throws NoSuchCityException {
        Map<String, Integer> routeMap = new HashMap<>();
        routeMap.put("shortDistance", Integer.MAX_VALUE);
        City cityA = this.getCityByName(a);
        City cityB = this.getCityByName(c);
        if (cityA == null || cityB == null)
            throw new NoSuchCityException();
        shortDistance(cityA, cityB, routeMap, 0, new ArrayList<>());
        return routeMap.get("shortDistance");
    }

    private void shortDistance(City cityA, City cityB, Map<String, Integer> routeMap, int distance, List<City> pasCities){
        List<Relation> relationFilter = relations.stream().filter(relation -> relation.getCityA().equals(cityA))
                .collect(Collectors.toList());
        if (relationFilter == null || relationFilter.size() <= 0)
            return;
        for (Relation relation: relationFilter){
            int value = distance + relation.getDistance();
            if (pasCities.contains(relation.getCityB())){
                continue;
            }
            List<City> itemPassCities = new ArrayList<>();
            itemPassCities.addAll(pasCities);
            itemPassCities.add(relation.getCityB());
            if (relation.getCityB().equals(cityB) ){
                if (value < routeMap.get("shortDistance")){
                    routeMap.put("shortDistance", value);
                }
                continue;
            }else if (!relation.getCityB().equals(cityB) && value >= routeMap.get("shortDistance")){
                continue;
            }else {
                shortDistance(relation.getCityB(), cityB, routeMap, value, itemPassCities);
            }
        }
    }

    @Override
    public int routeNumberForDistanceLessThanValue(String a, String c, int i) throws NoSuchCityException {
        Map<String, Integer> routeNumber = new HashMap<>();
        routeNumber.put("routeNumber", 0);
        City cityA = this.getCityByName(a);
        City cityB = this.getCityByName(c);
        if (cityA == null || cityB == null)
            throw new NoSuchCityException();
        routeNumberLessDistance(cityA, cityB, routeNumber, 0, 30);
        return routeNumber.get("routeNumber");
    }


    private void routeNumberLessDistance(City cityA, City cityB, Map<String, Integer> routeNumber, int distance, int maxDistance){
        List<Relation> relationFilter = relations.stream().filter(relation -> relation.getCityA().equals(cityA))
                .collect(Collectors.toList());
        if (relationFilter == null || relationFilter.size() <= 0)
            return;
        for (Relation relation: relationFilter){
            int value = distance + relation.getDistance();
            if (value >= maxDistance){
                continue;
            }
            if (relation.getCityB().equals(cityB) ){
                if (value < maxDistance){
                    routeNumber.put("routeNumber", routeNumber.get("routeNumber")+1);
                }
            }
            routeNumberLessDistance(relation.getCityB(), cityB, routeNumber, value, maxDistance);
        }
    }

    @Override
    public City getCityByName(String cityName) {
        if (cities == null || cities.size() < 1)
            return null;
        List<City> citiesFilter = cities.stream().filter(item -> cityName.equals(item.getCityName())).collect(Collectors.toList());
        if (citiesFilter.size() < 1)
            return null;
        return citiesFilter.get(0);
    }

    @Override
    public Relation getRelationByCities(City city, City pop) {
        if (relations == null || relations.size() <= 0)
            return null;

        List<Relation> relationFilter = relations.stream()
                .filter(relation -> relation.getCityA().equals(city) && relation.getCityB().equals(pop))
                .collect(Collectors.toList());
        if (relationFilter == null || relationFilter.size() <= 0)
            return null;
        return relationFilter.get(0);
    }
}
