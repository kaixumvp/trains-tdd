package com.heiiyo.relation.impl;

import com.heiiyo.city.City;
import com.heiiyo.relation.Relation;

public class BaseRelation implements Relation {

    private City cityA;
    private City cityB;
    private int distance;

    public BaseRelation(City cityA, City cityB, int distance) {
        this.cityA = cityA;
        this.cityB = cityB;
        this.distance = distance;
    }

    @Override
    public int getDistance() {
        return distance;
    }

    @Override
    public City getCityA() {
        return cityA;
    }

    @Override
    public City getCityB() {
        return cityB;
    }
}
