package com.heiiyo.relation;

import com.heiiyo.city.City;

public interface Relation {

    int getDistance();

    City getCityA();

    City getCityB();
}
